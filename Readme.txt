Please follow the below steps to understand the project:
1. In src folder there are 2 module- main and test and 2 directory- json and resource
2. main module has 2 classes- TeamConstant and TeamHelper
3. test module has 1 classes- IplTeamTests
4. json has 1 file folder where json response is saved
5. resource has xml file to run test from Jenkins
6. build.gradle has all the dependencies added