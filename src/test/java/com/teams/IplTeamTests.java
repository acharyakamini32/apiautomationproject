package com.teams;


import io.restassured.path.json.JsonPath;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.logging.Logger;


public class IplTeamTests {

    TeamHelper teamHelper = new TeamHelper();
    Logger logger = Logger.getLogger(IplTeamTests.class.getName());

    @Test
    public void validateForeignPlayersTest() throws IOException, ParseException {
        int count = 0;
        JsonPath jsonPath = new JsonPath(teamHelper.getCountOfPlayer());
        int getPlayersSize = jsonPath.getInt("player.size()");
        for (int i = 0; i < getPlayersSize; i++) {
            String country = jsonPath.get("player[" + i + "].country").toString();
            if (!country.equals(TeamConstant.COUNTRY_NAME)) {
                jsonPath.get("player[" + i + "].name").toString();
                count++;
            }
        }
        logger.info("Count of foreign" + "==" + count);
        Assert.assertEquals(count, 4);

    }

    @Test
    public void validateWicketKeeperTest() throws IOException, ParseException {
        int count = 0;
        JsonPath jsonPath = new JsonPath(teamHelper.getCountOfPlayer());
        int getPlayersSize = jsonPath.getInt("player.size()");
        for (int i = 0; i < getPlayersSize; i++) {
            String role = jsonPath.get("player[" + i + "].role").toString();
            if (role.equals(TeamConstant.ROLE)) {
                jsonPath.get("player[" + i + "].name").toString();
                count++;
            }
        }
        logger.info("Count of wicket-keeper" + "==" + count);
        Assert.assertEquals(count, 1);
    }
}



